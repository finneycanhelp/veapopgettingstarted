//
//  VPGAppDelegate.h
//  VeaPopGettingStarted
//
//  Created by Michael Finney on 7/13/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VPGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
