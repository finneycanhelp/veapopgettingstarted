//
//  main.m
//  VeaPopGettingStarted
//
//  Created by Michael Finney on 7/13/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VPGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VPGAppDelegate class]));
    }
}
